## Setup Laravel-API

Clone this repository.
```` 
git clone https://wahyusetiaone@bitbucket.org/whysetone/laravel-api.git
````
Go into app's directory
```` 
cd laravle-api 
````
After that, you must install all the dependencies which are needed.
```` 
composer install 
````
and
```` 
npm install 
````
## Database Config

Copy env.example to .env, then change this line to connection you already have.

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel_api
DB_USERNAME=root
DB_PASSWORD=
```

## Setup table and records

To generated table which one needed by this app, run this code.
```
php artisan migrate && php artisan db:seed --class=MoviesTableSeeder
```
### Have fun code
Before running this app, you must generated new key of this app.
```
php artisan key:generate && php artisan config:cache
```

Running this app using.
```
php artisan serve
```

## Noted
Communication between the backend and frontend in this application uses an API Connection, which does not pass data directly through the Laravel Controller. API endpoints are used as server side and WEB endpoints are used as client side. JavaScript on the client side is responsible for connecting to the server side.
It is can be found in this directory.
```
~laravel_api/resources/js
```

Server Side
Endpoints of API for this app is 

```
Route::get('movies', [MovieController::class, 'index']);
Route::post('movie', [MovieController::class, 'store']);
Route::get('movie/{id}', [MovieController::class, 'show']);
Route::put('movie/{id}', [MovieController::class, 'update']);
Route::delete('movie/{id}', [MovieController::class, 'destroy']);
```
And Client Side
endpoints of WEB URL is

```
Route::get('/', function () {
    return view('dashboard');
});

Route::get('/add', function () {
    return view('add');
});

Route::get('/show/{id}', function () {
    return view('show');
});

Route::get('/movies', function () {
    return view('dashboard');
});
```
## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
