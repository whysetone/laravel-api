<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laravel 8</title>
    <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>

<div class="container">

    <br/><br/>

    <div class="row">
        <div class="d-flex justify-content-center">
            <div class="col-10">
                <div class="card text-center">
                    <div class="card-header">
                        Lists of Movies
                        <br>
                        <button type="button" class="btn btn-default btn" id="add">
                            <i class="fas fa-pencil-alt">Add New Movie</i>
                        </button>
                    </div>
                    <div class="card-body">
                        <table class="table" id="tbl_movie">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name of Movie</th>
                                <th>Genre</th>
                                <th>Language</th>
                                <th>Rating</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer text-muted">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
{{--                                <li class="page-item disabled">--}}
{{--                                    <a class="page-link" href="#" tabindex="-1">Previous</a>--}}
{{--                                </li>--}}
{{--                                <li class="page-item"><a class="page-link" href="#">1</a></li>--}}
{{--                                <li class="page-item"><a class="page-link" href="#">2</a></li>--}}
{{--                                <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
{{--                                <li class="page-item">--}}
{{--                                    <a class="page-link" href="#">Next</a>--}}
{{--                                </li>--}}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ mix('js/api.js') }}"></script>
</body>
</html>
