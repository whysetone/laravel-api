<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laravel 8</title>
    <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>

<div class="container">

    <br/><br/>

    <div class="row">
        <div class="d-flex justify-content-center">
            <div class="col-10">
                <div class="card text-center">
                    <div class="card-header">
                        Featured
                    </div>
                    <div class="card-body">
                        <form name="data">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="title" class="form-label">Name of Movie</label>
                                        <input required="required" type="text" class="form-control" name="title" id="title">
                                    </div>
                                    <div class="mb-3">
                                        <label for="genre" class="form-label">Genre</label>
                                        <input required="required" type="text" class="form-control" name="genre" id="genre">
                                    </div>
                                    <div class="mb-3">
                                        <label for="languange" class="form-label">Language</label>
                                        <input required="required" type="text" class="form-control" name="languange" id="languange">
                                    </div>
                                    <div class="mb-3">
                                        <label for="vote_average" class="form-label">Rating</label>
                                        <input required="required" type=number step=0.1 class="form-control" name="vote_average"
                                               id="vote_average">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="overview" class="form-label">Overview</label>
                                        <textarea required="required" class="form-control" name="overview" id="overview"
                                                  rows="6"></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="popularity" class="form-label">Popularity</label>
                                        <input required="required" type=number step=0.001 class="form-control" name="popularity"
                                               id="popularity">
                                    </div>
                                    <div class="mb-3">
                                        <button type="button" class="btn btn-default btn-lg" id="back">
                                            <i class="fas fa-arrow-circle-left">Back</i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-lg" id="delete">
                                            <i class="fas fa-trash-alt">Delete</i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-lg" id="update">
                                            <i class="fas fa-pencil-alt">Update</i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer text-muted">
                        2 days ago
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ mix('js/api_show.js') }}"></script>
</body>
</html>
