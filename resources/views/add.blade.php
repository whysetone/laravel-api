<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laravel 8</title>
    <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>

<div class="container">

    <br/><br/>

    <div class="row">
        <div class="d-flex justify-content-center">
            <div class="col-10">
                <div class="card text-center">
                    <div class="card-header">
                        Add New Movie
                    </div>
                    <div class="card-body">
                        <form name="data">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="title" class="form-label">Name of Movie</label>
                                        <input required="required" type="text" class="form-control" name="title" id="title" placeholder="Kingkong">
                                    </div>
                                    <div class="mb-3">
                                        <label for="genre" class="form-label">Genre</label>
                                        <input required="required" type="text" class="form-control" name="genre" id="genre" placeholder="Action, Monster">
                                    </div>
                                    <div class="mb-3">
                                        <label for="languange" class="form-label">Language</label>
                                        <input required="required" type="text" class="form-control" name="languange" id="languange" placeholder="ID, EN">
                                    </div>
                                    <div class="mb-3">
                                        <label for="vote_average" class="form-label">Rating</label>
                                        <input required="required" type=number step=0.1 class="form-control" name="vote_average"
                                               id="vote_average" placeholder="8.9">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="overview" class="form-label">Overview</label>
                                        <textarea required="required" class="form-control" name="overview" id="overview"
                                                  rows="6" placeholder="This to description a movie"></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="popularity" class="form-label">Popularity</label>
                                        <input required="required" type=number step=0.001 class="form-control" name="popularity"
                                               id="popularity" placeholder="2801.900">
                                    </div>
                                    <div class="mb-3">
                                        <button type="button" class="btn btn-default btn-lg" id="back">
                                            <i class="fas fa-arrow-circle-left">Back</i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-lg" id="add">
                                            <i class="fas fa-save">Save</i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer text-muted">
                        Wahyu Setiawan
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ mix('js/api_add.js') }}"></script>
</body>
</html>
