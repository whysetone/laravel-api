$(document).ready(function () {
    var getUrl = window.location;
    var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    var baseUrl2 = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[0];
    var page_url = getUrl.pathname.split('/')[1];

    var url_string = window.location.href;
    var url = new URL(url_string);
    var pg = url.searchParams.get("page");
    if (page_url === "movies"){
        $.ajax({
            type: "GET",
            url: baseUrl2 + "api/movies?page="+pg,
            dataType: "json",
            success: function (result, status, xhr) {
                var table = $("#tbl_movie tbody");
                for (var x = 0; x < result.data.length; x++) {
                    table.append("<tr>");
                    table.append("<td>" + result.data[x].id + "</td>");
                    table.append("<td>" + result.data[x].title + "</td>");
                    table.append("<td>" + result.data[x].genre + "</td>");
                    table.append("<td>" + result.data[x].languange + "</td>");
                    table.append("<td>" + result.data[x].vote_average + "</td>");
                    table.append("<td>" +
                        "<a href='"+baseUrl2+"show/"+result.data[x].id+"'><i class=\"fas fa-eye\"></i></a>" +
                        " </td>");
                    table.append("</tr>");
                }

                var page_selector = $(".pagination");
                for (var page in result.meta.links){
                    var disabled = result.meta.links[page].url === null ? "disabled" : "";
                    var actived = result.meta.links[page].active === true ? "active" : "";
                    if (disabled !== "disabled"){
                        var urll_string = result.meta.links[page].url;
                        var urll = new URL(urll_string);
                        var pge = urll.searchParams.get("page");
                    }
                    page_selector.append("<li class=\"page-item "+disabled+" "+actived+"\"><a class=\"page-link\" href=\""+baseUrl2+"movies?page="+pge+"\">"+result.meta.links[page].label+"</a></li>");
                }
            },
            error: function (xhr, status, error) {
                Swal.fire('Oops...', status + " " + error + " " + xhr.status + " " + xhr.statusText, 'error');
            }
        });
    }else {
        $.ajax({
            type: "GET",
            url: baseUrl + "api/movies",
            dataType: "json",
            success: function (result, status, xhr) {
                var table = $("#tbl_movie tbody");
                for (var x = 0; x < result.data.length; x++) {
                    table.append("<tr>");
                    table.append("<td>" + result.data[x].id + "</td>");
                    table.append("<td>" + result.data[x].title + "</td>");
                    table.append("<td>" + result.data[x].genre + "</td>");
                    table.append("<td>" + result.data[x].languange + "</td>");
                    table.append("<td>" + result.data[x].vote_average + "</td>");
                    table.append("<td>" +
                        "<a href='"+baseUrl+"show/"+result.data[x].id+"'><i class=\"fas fa-eye\"></i></a>" +
                        " </td>");
                    table.append("</tr>");
                }

                var page_selector = $(".pagination");
                for (var page in result.meta.links){
                    var disabled = result.meta.links[page].url === null ? "disabled" : "";
                    var actived = result.meta.links[page].active === true ? "active" : "";
                    if (disabled !== "disabled"){
                        var urll_string = result.meta.links[page].url;
                        var urll = new URL(urll_string);
                        var pge = urll.searchParams.get("page");
                    }
                    page_selector.append("<li class=\"page-item "+disabled+" "+actived+"\"><a class=\"page-link\" href=\""+baseUrl+"movies?page="+pge+"\">"+result.meta.links[page].label+"</a></li>");
                }
            },
            error: function (xhr, status, error) {
                Swal.fire('Oops...', status + " " + error + " " + xhr.status + " " + xhr.statusText, 'error');
            }
        });
    }


    $("#add").click(function () {
        if (page_url === "movies") {
            window.location = baseUrl2 + 'add';
        }else {
            window.location = baseUrl + 'add';
        }
    });
});
