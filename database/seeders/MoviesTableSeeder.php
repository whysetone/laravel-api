<?php

namespace Database\Seeders;

use App\Models\Movie;
use Illuminate\Database\Seeder;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movie = new Movie;
        $movie->genre = 'Horor';
        $movie->languange = 'en';
        $movie->title = 'Luca';
        $movie->overview = 'Luca and his best friend Alberto experience an unforgettable summer on the Italian Riviera. But all the fun is threatened by a deeply-held secret: they are sea monsters from another world just below the water’s surface.';
        $movie->popularity = 7169.114;
        $movie->vote_average = 8.2;
        $movie->save();

        $movie_1 = new Movie;
        $movie_1->genre = 'Drama';
        $movie_1->languange = 'en';
        $movie_1->title = 'Infinite';
        $movie_1->overview = 'Evan McCauley has skills he never learned and memories of places he has never visited. Self-medicated and on the brink of a mental breakdown, a secret group that call themselves “Infinites” come to his rescue, revealing that his memories are real.';
        $movie_1->popularity = 5018.635;
        $movie_1->vote_average = 0;
        $movie_1->save();

        $movie_2 = new Movie;
        $movie_2->genre = 'Drama';
        $movie_2->languange = 'en';
        $movie_2->title = 'A Quiet Place Part II';
        $movie_2->overview = 'Following the events at home, the Abbott family now face the terrors of the outside world. Forced to venture into the unknown, they realize that the creatures that hunt by sound are not the only threats that lurk beyond the sand path.';
        $movie_2->popularity = 5250.562;
        $movie_2->vote_average = 7.7;
        $movie_2->save();

        $movie_3 = new Movie;
        $movie_3->genre = 'Horor';
        $movie_3->languange = 'en';
        $movie_3->title = 'Luca and Loxi';
        $movie_3->overview = 'Luca and his best friend Alberto experience an unforgettable summer on the Italian Riviera. But all the fun is threatened by a deeply-held secret: they are sea monsters from another world just below the water’s surface.';
        $movie_3->popularity = 7169.114;
        $movie_3->vote_average = 8.2;
        $movie_3->save();

        $movie_4 = new Movie;
        $movie_4->genre = 'Horor';
        $movie_4->languange = 'en';
        $movie_4->title = 'A Quiet Place';
        $movie_4->overview = 'Following the events at home, the Abbott family now face the terrors of the outside world. Forced to venture into the unknown, they realize that the creatures that hunt by sound are not the only threats that lurk beyond the sand path.';
        $movie_4->popularity = 5250.562;
        $movie_4->vote_average = 7.7;
        $movie_4->save();

        $movie_5 = new Movie;
        $movie_5->genre = 'Drama';
        $movie_5->languange = 'en';
        $movie_5->title = 'Infinite 2';
        $movie_5->overview = 'Evan McCauley has skills he never learned and memories of places he has never visited. Self-medicated and on the brink of a mental breakdown, a secret group that call themselves “Infinites” come to his rescue, revealing that his memories are real.';
        $movie_5->popularity = 5018.635;
        $movie_5->vote_average = 0;
        $movie_5->save();

        $movie_6 = new Movie;
        $movie_6->genre = 'Horor';
        $movie_6->languange = 'en';
        $movie_6->title = 'A Quiet Place Part I';
        $movie_6->overview = 'Following the events at home, the Abbott family now face the terrors of the outside world. Forced to venture into the unknown, they realize that the creatures that hunt by sound are not the only threats that lurk beyond the sand path.';
        $movie_6->popularity = 5250.562;
        $movie_6->vote_average = 7.7;
        $movie_6->save();
    }
}
