<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|\Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::orderBy('id', 'asc')->paginate(5);
        return \App\Http\Resources\Movie::collection($movies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Movie
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Http\Resources\Movie|Movie|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movie = new Movie();
        $movie->genre = $request->input('genre');
        $movie->languange = $request->input('languange');
        $movie->title = $request->input('title');
        $movie->overview = $request->input('overview');
        $movie->popularity = $request->input('popularity');
        $movie->vote_average = $request->input('vote_average');

        if($movie->save()) {
            return new \App\Http\Resources\Movie($movie);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \App\Http\Resources\Movie|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $movie = Movie::findOrFail($id);
        return new \App\Http\Resources\Movie($movie);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $movie = Movie::findOrFail($id);
        $movie->genre = $request->input('genre');
        $movie->languange = $request->input('languange');
        $movie->title = $request->input('title');
        $movie->overview = $request->input('overview');
        $movie->popularity = $request->input('popularity');
        $movie->vote_average = $request->input('vote_average');

        if($movie->save()) {
            return new \App\Http\Resources\Movie($movie);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \App\Http\Resources\Movie|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movie = Movie::findOrFail($id);
        if($movie->delete()) {
            return new \App\Http\Resources\Movie($movie);
        }
    }
}
