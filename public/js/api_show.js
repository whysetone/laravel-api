/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**********************************!*\
  !*** ./resources/js/api_show.js ***!
  \**********************************/
$(document).ready(function () {
  var getUrl = window.location;
  var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[0];
  var id = getUrl.pathname.split('/')[2];
  $.ajax({
    type: "GET",
    url: baseUrl + "api/movie/" + id,
    dataType: "json",
    success: function success(result, status, xhr) {
      $(".card-header").text("Details of " + result.data.title);
      $("#title").val(result.data.title);
      $("#genre").val(result.data.genre);
      $("#languange").val(result.data.languange);
      $("#overview").val(result.data.overview);
      $("#popularity").val(result.data.popularity);
      $("#vote_average").val(result.data.vote_average);
      $(".card-footer, .text-muted").text(result.data.updated_at);
    },
    error: function error(xhr, status, _error) {
      alert("Result: " + status + " " + _error + " " + xhr.status + " " + xhr.statusText);
    }
  });
  $("#delete").click(function () {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this record!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then(function (result) {
      if (result.isConfirmed) {
        $.ajax({
          type: "delete",
          url: baseUrl + "api/movie/" + id,
          dataType: "json",
          success: function success(result, status, xhr) {
            Swal.fire('Deleted!', 'Record has been deleted.', 'success').then(function () {
              window.location = baseUrl;
            });
          },
          error: function error(xhr, status, _error2) {
            Swal.fire('Oops...', status + " " + _error2 + " " + xhr.status + " " + xhr.statusText, 'error');
          }
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', 'Delete record has been cancelled !', 'error');
      }
    });
  });
  $("#update").click(function () {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will update this record ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, send it!',
      cancelButtonText: 'No, keep it'
    }).then(function (result) {
      if (result.isConfirmed) {
        $.ajax({
          type: "put",
          url: baseUrl + "api/movie/" + id,
          data: $('form').serialize(),
          dataType: "json",
          success: function success(result, status, xhr) {
            Swal.fire('Updated!', 'Record has been Updated !!!', 'success');
          },
          error: function error(xhr, status, _error3) {
            Swal.fire('Oops...', status + " " + _error3 + " " + xhr.status + " " + xhr.statusText, 'error');
          }
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', 'Update record has been cancelled !', 'error');
      }
    });
  });
  $("#back").click(function () {
    window.location = baseUrl;
  });
});
/******/ })()
;