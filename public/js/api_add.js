/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*********************************!*\
  !*** ./resources/js/api_add.js ***!
  \*********************************/
$(document).ready(function () {
  var getUrl = window.location;
  var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[0];
  $("#add").click(function () {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will save this record ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, save it!',
      cancelButtonText: 'No, clear it'
    }).then(function (result) {
      if (result.isConfirmed) {
        $.ajax({
          type: "post",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: baseUrl + "api/movie",
          data: $('form').serialize(),
          dataType: "json",
          success: function success(result, status, xhr) {
            Swal.fire('Saved!', 'Record has been Saved !!!', 'success').then(function () {
              window.location = baseUrl;
            });
          },
          error: function error(xhr, status, _error) {
            Swal.fire('Oops...', status + " " + _error + " " + xhr.status + " " + xhr.statusText, 'error');
          }
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Cancelled', 'Saved record has been cancelled !', 'error');
      }
    });
  });
  $("#back").click(function () {
    window.location = baseUrl;
  });
});
/******/ })()
;